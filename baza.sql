-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 16 Lut 2020, 13:09
-- Wersja serwera: 10.2.29-MariaDB
-- Wersja PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `esport`
--
CREATE DATABASE IF NOT EXISTS `esport` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `esport`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `druzyny`
--

DROP TABLE IF EXISTS `druzyny`;
CREATE TABLE `druzyny` (
  `id_druzyna` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `data_rozpoczecie` date NOT NULL,
  `data_zakonczenie` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `druzyny_gracze`
--

DROP TABLE IF EXISTS `druzyny_gracze`;
CREATE TABLE `druzyny_gracze` (
  `id_druzyna` bigint(20) UNSIGNED NOT NULL,
  `id_gracz` bigint(20) UNSIGNED NOT NULL,
  `data_dolaczenie` date NOT NULL,
  `data_zakonczenie` date DEFAULT NULL COMMENT 'uzupełniamy, gdy gracz kończy współpracę z drużyną'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `druzyny_gracze_role`
--

DROP TABLE IF EXISTS `druzyny_gracze_role`;
CREATE TABLE `druzyny_gracze_role` (
  `id_gracz` bigint(20) UNSIGNED NOT NULL,
  `id_rola` bigint(20) UNSIGNED NOT NULL,
  `id_druzyna` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gracze`
--

DROP TABLE IF EXISTS `gracze`;
CREATE TABLE `gracze` (
  `id_gracz` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `id_gra` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gry`
--

DROP TABLE IF EXISTS `gry`;
CREATE TABLE `gry` (
  `id_gra` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(40) NOT NULL,
  `gatunek` varchar(15) NOT NULL,
  `tryb_gry` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_pocztowe`
--

DROP TABLE IF EXISTS `kody_pocztowe`;
CREATE TABLE `kody_pocztowe` (
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `nazwa` char(6) NOT NULL,
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kraje`
--

DROP TABLE IF EXISTS `kraje`;
CREATE TABLE `kraje` (
  `id_kraj` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kraje`
--

INSERT INTO `kraje` (`id_kraj`, `nazwa`) VALUES
(1, 'Polska'),
(2, 'Great Britain'),
(3, 'USA'),
(4, 'France'),
(5, 'Hungary'),
(6, 'Russia'),
(7, 'Germay');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lokalizacje`
--

DROP TABLE IF EXISTS `lokalizacje`;
CREATE TABLE `lokalizacje` (
  `id_lokalizacja` bigint(20) UNSIGNED NOT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `numer_budynek` varchar(10) NOT NULL,
  `numer_lokal` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mecze`
--

DROP TABLE IF EXISTS `mecze`;
CREATE TABLE `mecze` (
  `id_mecz` bigint(20) UNSIGNED NOT NULL,
  `data_rozpoczecie` datetime NOT NULL,
  `data_zakonczenie` datetime DEFAULT NULL,
  `id_turniej` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mecze_druzyny`
--

DROP TABLE IF EXISTS `mecze_druzyny`;
CREATE TABLE `mecze_druzyny` (
  `id_mecz` bigint(20) UNSIGNED NOT NULL,
  `id_druzyna` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miejscowosci`
--

DROP TABLE IF EXISTS `miejscowosci`;
CREATE TABLE `miejscowosci` (
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(70) NOT NULL,
  `id_kraj` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `miejscowosci`
--

INSERT INTO `miejscowosci` (`id_miejscowosc`, `nazwa`, `id_kraj`) VALUES
(3, 'Bydgoszcz', 1),
(4, 'Warszawa', 1),
(5, 'Częstochowa', 1),
(6, 'Częstochowa', 1),
(7, 'Gdańsk', 1),
(8, 'Gdynia', 1),
(9, 'Berlin', 7),
(10, 'Drezno', 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(40) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `data_urodzenia` date NOT NULL,
  `emial` varchar(100) NOT NULL,
  `telefon` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id_rola` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sponsorzy`
--

DROP TABLE IF EXISTS `sponsorzy`;
CREATE TABLE `sponsorzy` (
  `id_sponsor` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `id_lokalizacja` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefon` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `trenerzy`
--

DROP TABLE IF EXISTS `trenerzy`;
CREATE TABLE `trenerzy` (
  `id_trener` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `aktywny` tinyint(1) NOT NULL,
  `id_lokalizacja` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `turnieje`
--

DROP TABLE IF EXISTS `turnieje`;
CREATE TABLE `turnieje` (
  `id_turniej` bigint(20) UNSIGNED NOT NULL,
  `data_turnieju` datetime NOT NULL,
  `czas_trwania` int(3) NOT NULL DEFAULT 1,
  `id_lokalizacja` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `turnieje_druzyny`
--

DROP TABLE IF EXISTS `turnieje_druzyny`;
CREATE TABLE `turnieje_druzyny` (
  `id_turniej` bigint(20) UNSIGNED NOT NULL,
  `id_druzyna` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `turnieje_sponsorzy`
--

DROP TABLE IF EXISTS `turnieje_sponsorzy`;
CREATE TABLE `turnieje_sponsorzy` (
  `id_turniej` bigint(20) UNSIGNED NOT NULL,
  `id_sponsor` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ulice`
--

DROP TABLE IF EXISTS `ulice`;
CREATE TABLE `ulice` (
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `ulice`
--

INSERT INTO `ulice` (`id_ulica`, `nazwa`) VALUES
(1, 'Kwiatowa'),
(2, 'Polna'),
(3, 'Jana Pawła II'),
(4, 'Garibaldiego'),
(5, 'Żwirki i Wigury'),
(6, 'Pułaskiego');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wyniki`
--

DROP TABLE IF EXISTS `wyniki`;
CREATE TABLE `wyniki` (
  `id_wynik` bigint(20) UNSIGNED NOT NULL,
  `id_mecz` bigint(20) UNSIGNED NOT NULL,
  `id_druzyna` bigint(20) UNSIGNED NOT NULL,
  `wynik` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `druzyny`
--
ALTER TABLE `druzyny`
  ADD UNIQUE KEY `id_druzyna` (`id_druzyna`);

--
-- Indeksy dla tabeli `druzyny_gracze`
--
ALTER TABLE `druzyny_gracze`
  ADD KEY `id_druzyna` (`id_druzyna`),
  ADD KEY `id_gracz` (`id_gracz`);

--
-- Indeksy dla tabeli `druzyny_gracze_role`
--
ALTER TABLE `druzyny_gracze_role`
  ADD KEY `id_gracz` (`id_gracz`),
  ADD KEY `id_rola` (`id_rola`),
  ADD KEY `id_druzyna` (`id_druzyna`);

--
-- Indeksy dla tabeli `gracze`
--
ALTER TABLE `gracze`
  ADD UNIQUE KEY `id_gracz` (`id_gracz`),
  ADD UNIQUE KEY `id_osoba` (`id_osoba`) USING BTREE,
  ADD KEY `id_gra` (`id_gra`);

--
-- Indeksy dla tabeli `gry`
--
ALTER TABLE `gry`
  ADD UNIQUE KEY `id_gra` (`id_gra`);

--
-- Indeksy dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD UNIQUE KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indeksy dla tabeli `kraje`
--
ALTER TABLE `kraje`
  ADD UNIQUE KEY `id_kraj` (`id_kraj`);

--
-- Indeksy dla tabeli `lokalizacje`
--
ALTER TABLE `lokalizacje`
  ADD UNIQUE KEY `id_lokalizacja` (`id_lokalizacja`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_ulica` (`id_ulica`);

--
-- Indeksy dla tabeli `mecze`
--
ALTER TABLE `mecze`
  ADD UNIQUE KEY `id_mecz` (`id_mecz`),
  ADD KEY `id_turniej` (`id_turniej`);

--
-- Indeksy dla tabeli `mecze_druzyny`
--
ALTER TABLE `mecze_druzyny`
  ADD KEY `id_mecz` (`id_mecz`),
  ADD KEY `id_druzyna` (`id_druzyna`);

--
-- Indeksy dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD UNIQUE KEY `id_miejscowosc` (`id_miejscowosc`),
  ADD KEY `id_kraj` (`id_kraj`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD PRIMARY KEY (`id_osoba`);

--
-- Indeksy dla tabeli `role`
--
ALTER TABLE `role`
  ADD UNIQUE KEY `id_rola` (`id_rola`);

--
-- Indeksy dla tabeli `sponsorzy`
--
ALTER TABLE `sponsorzy`
  ADD UNIQUE KEY `id_sponsor` (`id_sponsor`),
  ADD KEY `id_lokalizacja` (`id_lokalizacja`);

--
-- Indeksy dla tabeli `trenerzy`
--
ALTER TABLE `trenerzy`
  ADD UNIQUE KEY `id_trener` (`id_trener`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_lokalizacja` (`id_lokalizacja`);

--
-- Indeksy dla tabeli `turnieje`
--
ALTER TABLE `turnieje`
  ADD UNIQUE KEY `id_turniej` (`id_turniej`),
  ADD KEY `id_lokalizacja` (`id_lokalizacja`);

--
-- Indeksy dla tabeli `turnieje_druzyny`
--
ALTER TABLE `turnieje_druzyny`
  ADD KEY `id_turniej` (`id_turniej`),
  ADD KEY `id_druzyna` (`id_druzyna`);

--
-- Indeksy dla tabeli `turnieje_sponsorzy`
--
ALTER TABLE `turnieje_sponsorzy`
  ADD KEY `id_turniej` (`id_turniej`),
  ADD KEY `id_sponsor` (`id_sponsor`);

--
-- Indeksy dla tabeli `ulice`
--
ALTER TABLE `ulice`
  ADD UNIQUE KEY `id_ulica` (`id_ulica`);

--
-- Indeksy dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  ADD UNIQUE KEY `id_wynik` (`id_wynik`),
  ADD KEY `id_mecz` (`id_mecz`),
  ADD KEY `id_druzyna` (`id_druzyna`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `druzyny`
--
ALTER TABLE `druzyny`
  MODIFY `id_druzyna` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gracze`
--
ALTER TABLE `gracze`
  MODIFY `id_gracz` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gry`
--
ALTER TABLE `gry`
  MODIFY `id_gra` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  MODIFY `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kraje`
--
ALTER TABLE `kraje`
  MODIFY `id_kraj` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `lokalizacje`
--
ALTER TABLE `lokalizacje`
  MODIFY `id_lokalizacja` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `mecze`
--
ALTER TABLE `mecze`
  MODIFY `id_mecz` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  MODIFY `id_miejscowosc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `role`
--
ALTER TABLE `role`
  MODIFY `id_rola` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `sponsorzy`
--
ALTER TABLE `sponsorzy`
  MODIFY `id_sponsor` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `trenerzy`
--
ALTER TABLE `trenerzy`
  MODIFY `id_trener` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `turnieje`
--
ALTER TABLE `turnieje`
  MODIFY `id_turniej` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ulice`
--
ALTER TABLE `ulice`
  MODIFY `id_ulica` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  MODIFY `id_wynik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `druzyny_gracze`
--
ALTER TABLE `druzyny_gracze`
  ADD CONSTRAINT `druzyny_gracze_ibfk_1` FOREIGN KEY (`id_druzyna`) REFERENCES `druzyny` (`id_druzyna`),
  ADD CONSTRAINT `druzyny_gracze_ibfk_2` FOREIGN KEY (`id_gracz`) REFERENCES `gracze` (`id_gracz`);

--
-- Ograniczenia dla tabeli `druzyny_gracze_role`
--
ALTER TABLE `druzyny_gracze_role`
  ADD CONSTRAINT `druzyny_gracze_role_ibfk_1` FOREIGN KEY (`id_druzyna`) REFERENCES `druzyny` (`id_druzyna`),
  ADD CONSTRAINT `druzyny_gracze_role_ibfk_2` FOREIGN KEY (`id_gracz`) REFERENCES `gracze` (`id_gracz`),
  ADD CONSTRAINT `druzyny_gracze_role_ibfk_3` FOREIGN KEY (`id_rola`) REFERENCES `role` (`id_rola`);

--
-- Ograniczenia dla tabeli `gracze`
--
ALTER TABLE `gracze`
  ADD CONSTRAINT `gracze_ibfk_1` FOREIGN KEY (`id_gra`) REFERENCES `gry` (`id_gra`),
  ADD CONSTRAINT `gracze_ibfk_2` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD CONSTRAINT `kody_pocztowe_ibfk_1` FOREIGN KEY (`id_miejscowosc`) REFERENCES `miejscowosci` (`id_miejscowosc`);

--
-- Ograniczenia dla tabeli `lokalizacje`
--
ALTER TABLE `lokalizacje`
  ADD CONSTRAINT `lokalizacje_ibfk_1` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`),
  ADD CONSTRAINT `lokalizacje_ibfk_2` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`);

--
-- Ograniczenia dla tabeli `mecze`
--
ALTER TABLE `mecze`
  ADD CONSTRAINT `mecze_ibfk_1` FOREIGN KEY (`id_turniej`) REFERENCES `turnieje` (`id_turniej`);

--
-- Ograniczenia dla tabeli `mecze_druzyny`
--
ALTER TABLE `mecze_druzyny`
  ADD CONSTRAINT `mecze_druzyny_ibfk_1` FOREIGN KEY (`id_druzyna`) REFERENCES `druzyny` (`id_druzyna`),
  ADD CONSTRAINT `mecze_druzyny_ibfk_2` FOREIGN KEY (`id_mecz`) REFERENCES `mecze` (`id_mecz`);

--
-- Ograniczenia dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD CONSTRAINT `miejscowosci_ibfk_1` FOREIGN KEY (`id_kraj`) REFERENCES `kraje` (`id_kraj`);

--
-- Ograniczenia dla tabeli `sponsorzy`
--
ALTER TABLE `sponsorzy`
  ADD CONSTRAINT `sponsorzy_ibfk_1` FOREIGN KEY (`id_lokalizacja`) REFERENCES `lokalizacje` (`id_lokalizacja`);

--
-- Ograniczenia dla tabeli `trenerzy`
--
ALTER TABLE `trenerzy`
  ADD CONSTRAINT `trenerzy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `trenerzy_ibfk_2` FOREIGN KEY (`id_lokalizacja`) REFERENCES `lokalizacje` (`id_lokalizacja`);

--
-- Ograniczenia dla tabeli `turnieje`
--
ALTER TABLE `turnieje`
  ADD CONSTRAINT `turnieje_ibfk_1` FOREIGN KEY (`id_lokalizacja`) REFERENCES `lokalizacje` (`id_lokalizacja`);

--
-- Ograniczenia dla tabeli `turnieje_druzyny`
--
ALTER TABLE `turnieje_druzyny`
  ADD CONSTRAINT `turnieje_druzyny_ibfk_1` FOREIGN KEY (`id_druzyna`) REFERENCES `druzyny` (`id_druzyna`),
  ADD CONSTRAINT `turnieje_druzyny_ibfk_2` FOREIGN KEY (`id_turniej`) REFERENCES `turnieje` (`id_turniej`);

--
-- Ograniczenia dla tabeli `turnieje_sponsorzy`
--
ALTER TABLE `turnieje_sponsorzy`
  ADD CONSTRAINT `turnieje_sponsorzy_ibfk_1` FOREIGN KEY (`id_turniej`) REFERENCES `turnieje` (`id_turniej`),
  ADD CONSTRAINT `turnieje_sponsorzy_ibfk_2` FOREIGN KEY (`id_sponsor`) REFERENCES `sponsorzy` (`id_sponsor`);

--
-- Ograniczenia dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  ADD CONSTRAINT `wyniki_ibfk_1` FOREIGN KEY (`id_mecz`) REFERENCES `mecze` (`id_mecz`),
  ADD CONSTRAINT `wyniki_ibfk_2` FOREIGN KEY (`id_druzyna`) REFERENCES `druzyny` (`id_druzyna`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
